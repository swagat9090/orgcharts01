﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeDetails.aspx.cs" Inherits="OrgChart.EmployeeDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 300px;
            float: left;
            margin: 137px 204px;
            text-align: center;
            font-family: arial;
        }

        .title {
            color: grey;
            font-size: 18px;
        }

        button {
            border: none;
            outline: 0;
            display: inline-block;
            padding: 8px;
            color: white;
            background-color: #000;
            text-align: center;
            cursor: pointer;
            width: 100%;
            font-size: 18px;
        }

        a {
            text-decoration: none;
            font-size: 22px;
            color: black;
        }

            button:hover, a:hover {
                opacity: 0.7;
            }
    </style>
    <script>

        var data = "";

            $(document).ready(function () {

            $.ajax({
                type: "POST",
                url: "EmployeeDetails.aspx/BindDetails",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    data = response.d;
                    $('#EmployeeName').html(response.d[0].EmployeeName);
                    $('#Title').html(response.d[0].Designation);
                    $('#CompanyName').html(response.d[0].CompanyName);
                    $('#Contact').html(response.d[0].Contact);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });

        //function myContact() {
        //    $('#btnContact').attr('value', data[0].Contact);
        //}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="text-align: center">Employee Details</h2>
    <div class="card">
        <img src="Images/boy-512.png" alt="ProfPic" style="width: 100%; background-color:lightgray">
        <h1 id="EmployeeName"></h1>
        <p class="title" id="Title"></p>
        <p id="CompanyName"></p>
        <p>
            <span id="Contact">Contact</span>
        </p>
    </div>
</asp:Content>
