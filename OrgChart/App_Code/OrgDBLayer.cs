﻿using DBLibrary;
using OrgChart.App_Code;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public partial class DBLayer
{
    public List<DropDownData> BindCompany(string QueryType)
    {
        List<DropDownData> lstData = new List<DropDownData>();

        SqlDataReader sqlRdr = null;

        string Constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        try
        {
            string ProcName = "spGetCompany";

            SqlParameter[] paramlist = null;
            paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@QueryType", QueryType);

            sqlRdr = SqlHelper.ExecuteReader(Constr, CommandType.StoredProcedure, ProcName, paramlist);

            while (sqlRdr.Read())
            {
                DropDownData ddl = new DropDownData();

                ddl.CompanyId = Convert.ToInt64(sqlRdr["Company_ID"]);
                ddl.CompanyName = sqlRdr["Company_Name"].ToString();

                lstData.Add(ddl);
            }

        }
        catch(Exception ex)
        {

        }
        finally
        {

        }

        return lstData;
    }

    public List<DepartmentDropDown> BindDepartment(string QueryType, int CompanyId)
    {
        List<DepartmentDropDown> lstData = new List<DepartmentDropDown>();

        SqlDataReader sqlRdr = null;

        string Constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        try
        {
            string ProcName = "spGetCompany";

            SqlParameter[] paramlist = null;
            paramlist = new SqlParameter[2];
            paramlist[0] = new SqlParameter("@QueryType", QueryType);
            paramlist[1] = new SqlParameter("@CompanyId", CompanyId);

            sqlRdr = SqlHelper.ExecuteReader(Constr, CommandType.StoredProcedure, ProcName, paramlist);

            while (sqlRdr.Read())
            {
                DepartmentDropDown ddl = new DepartmentDropDown();

                ddl.Department = sqlRdr["Employee_Departments"].ToString();

                lstData.Add(ddl);
            }

        }
        catch (Exception ex)
        {

        }
        finally
        {

        }

        return lstData;
    }

    public List<OrgData> DrawChart(int companyid, string department)
    {
        List<OrgData> lstData = new List<OrgData>();

        SqlDataReader sqlRdr = null;

        string Constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        string sqlCom = "";

        try
        {

            using (SqlConnection conn = new SqlConnection(Constr))
            {
                conn.Open();
                sqlCom += "SELECT A.EMPLOYEE_ID AS EmployeeId, A.Employee_FullName AS EmployeeName ,A.Employee_Title AS Designation,";  
                sqlCom += "CASE WHEN A.Count IS NULL THEN 0 ELSE A.Count END AS Count ,";
                sqlCom += "CASE WHEN A.Employee_Parent_ID IS NULL THEN 0 ELSE A.Employee_Parent_ID END AS ParentId ,";
                sqlCom += "CASE WHEN A.Employee_Parent_FullName IS NULL THEN '' ELSE A.Employee_Parent_FullName END AS ParentName ";
                sqlCom += "FROM CCONTACTS A LEFT JOIN CContacts B ON B.Employee_ID = A.Employee_Parent_ID WHERE A.Company_ID = " + companyid + " AND A.Employee_Departments in (" + department + ")";

                SqlCommand cmd = new SqlCommand(sqlCom, conn);
                sqlRdr = cmd.ExecuteReader();

                while (sqlRdr.Read())
                {
                    OrgData ddl = new OrgData();

                    ddl.EmployeeId = Convert.ToInt64(sqlRdr["EmployeeId"]);
                    ddl.EmployeeName = sqlRdr["EmployeeName"].ToString();
                    ddl.Designation = sqlRdr["Designation"].ToString();
                    ddl.Count = Convert.ToInt64(sqlRdr["Count"]);
                    ddl.ParentId = Convert.ToInt64(sqlRdr["ParentId"]);
                    ddl.ParentName = sqlRdr["ParentName"].ToString();

                    lstData.Add(ddl);
                }
            }


        }
        catch (Exception ex)
        {

        }
        finally
        {

        }

        return lstData;
    }

    public List<EmployeeView> GetDetails(string EmployeeId)
    {
        List<EmployeeView> lstData = new List<EmployeeView>();

        SqlDataReader sqlRdr = null;

        string Constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        try
        {
            string ProcName = "spGetEmployeeDetails";

            SqlParameter[] paramlist = null;
            paramlist = new SqlParameter[1];
            paramlist[0] = new SqlParameter("@employeeid", EmployeeId.ToString());
            //paramlist[1] = new SqlParameter("@CompanyId", CompanyId);

            sqlRdr = SqlHelper.ExecuteReader(Constr, CommandType.StoredProcedure, ProcName, paramlist);

            while (sqlRdr.Read())
            {
                EmployeeView ddl = new EmployeeView();

                ddl.EmployeeName = sqlRdr["Employee_FullName"].ToString();
                ddl.Designation = sqlRdr["Employee_Title"].ToString();
                ddl.CompanyName = sqlRdr["Company_Name"].ToString();
                ddl.Contact = sqlRdr["Employee_Direct_Phone"].ToString();

                lstData.Add(ddl);
            }

        }
        catch (Exception ex)
        {

        }
        finally
        {

        }

        return lstData;
    }
}