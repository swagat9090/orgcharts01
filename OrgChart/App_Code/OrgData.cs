﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrgChart.App_Code
{
    public class DropDownData
    {
        public Int64 CompanyId { get; set; }
        public string CompanyName { get; set; }
    }

    public class DepartmentDropDown
    {
        public string Department { get; set; }
    }

    public class OrgData
    {
        public Int64 EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public Int64 Count { get; set; }
        public Int64 ParentId { get; set; }
        public string ParentName { get; set; }
    }

    public class EmployeeView
    {
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string CompanyName { get; set; }

        public string Contact { get; set; }
    }
}