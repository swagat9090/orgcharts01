﻿using OrgChart.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrgChart
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod(EnableSession = true)]
        public static List<DropDownData> PopulateCompany(string querytype)
        {
            List<DropDownData> NewList = new List<DropDownData>();
            try
            {
                DBLayer db = new DBLayer();
                NewList = db.BindCompany(querytype.ToString());
            }
            catch
            {
                NewList = null;
            }

            return NewList;
        }

        [WebMethod(EnableSession = true)]
        public static List<DepartmentDropDown> PopulateDepartment(string querytype, string companyid)
        {
            List<DepartmentDropDown> DepartmentList = new List<DepartmentDropDown>();
            try
            {
                DBLayer db = new DBLayer();
                DepartmentList = db.BindDepartment(querytype, Convert.ToInt32(companyid));
            }
            catch
            {
                DepartmentList = null;
            }

            return DepartmentList;
        }

        [WebMethod(EnableSession = true)]
        public static List<OrgData> DrawOrgChart(string companyid, string department)
        {
            List<OrgData> NewList = new List<OrgData>();
            try
            {
                DBLayer db = new DBLayer();
                NewList = db.DrawChart(Convert.ToInt32(companyid), department);
            }
            catch
            {
                NewList = null;
            }

            return NewList;
        }

        [WebMethod(EnableSession = true)]
        public static string EmployeeSession(int employeeid)
        {
            try
            {
                HttpContext.Current.Session["EmployeeId"] = employeeid;
            }
            catch
            {
                
            }

            return "";
        }
    }
}