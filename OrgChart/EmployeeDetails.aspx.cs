﻿using OrgChart.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrgChart
{
    public partial class EmployeeDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod(EnableSession = true)]
        public static List<EmployeeView> BindDetails()
        {
            List<EmployeeView> lst = new List<EmployeeView>();
            string EmployeeId = HttpContext.Current.Session["EmployeeId"].ToString();
            try
            {
                DBLayer db = new DBLayer();
                lst = db.GetDetails(EmployeeId);
            }
            catch
            {
                lst = null;
            }

            return lst;
        }
    }
}