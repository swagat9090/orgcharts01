﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OrgChart._Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
        rel="stylesheet" type="text/css" />
    <script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
        type="text/javascript"></script>
    <script>
        var orgdata;

        google.charts.load('current', {packages:["orgchart"]});

        $(document).ready(function () {
            $("#divdept").css("display", "none");

            $.ajax({
                type: "POST",
                url: "Default.aspx/PopulateCompany",
                data: '{"querytype":"BindCompany"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {


                    $("#ddlCompany").empty();
                    $("#ddlCompany").append($("<option/>").val("0").text("Select"));
                    $.each(response.d, function () {
                        $("#ddlCompany").append($("<option/>").val(this.CompanyId).text(this.CompanyName));
                    });

                    //$("#Content").text(response.d);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });

        function BindDepartments() {
            $("#divdept").css("display", "block");
            var companyid = $("#ddlCompany").val();

            $.ajax({
                type: "POST",
                url: "Default.aspx/PopulateDepartment",
                data: '{"querytype":"BindDepartment","companyid":"' + companyid + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#ddlDepartment").empty();
                    $.each(response.d, function () {
                        $("#ddlDepartment").append($("<option/>").val(this.Department).text(this.Department));
                    });
                    $('#ddlDepartment').multiselect({
                        includeSelectAllOption: true
                    });
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function DrawChart() {
            var companyid = $("#ddlCompany").val();
            var ddldepartment = $("#ddlDepartment option:selected");

            var department = "";

            ddldepartment.each(function () {
                department += "'" + $(this).text() + "',";
            });

            department = department.replace(/,\s*$/, "");

            $.ajax({
                type: "POST",
                url: "Default.aspx/DrawOrgChart",
                data: '{"companyid":"' + companyid + '","department":"' + department + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //orgdata = data.d;
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Name');
                    data.addColumn('string', 'Manager');
                    data.addColumn('string', 'ToolTip');

                    // For each orgchart box, provide the name, manager, and tooltip to show.
                    for (var i = 0; i < response.d.length; i++) {
                        data.addRows([
                            [{ v: response.d[i].EmployeeName, f: '<a href="javascript:void(0)" style="color:black; font-size:12px; text-decoration:none" onclick="ViewDetails('+ response.d[i].EmployeeId +');">'+ response.d[i].EmployeeName + '<div><div style="color:red; font-style:italic">' + response.d[i].Designation + '</div><div><span style = "height: 25px; width: 25px; background-color: #bbb; border-radius: 50%; display: inline-block; padding:5px;">' + response.d[i].Count + '</span></div></div></a>' },
                            response.d[i].ParentName, response.d[i].EmployeeName + '\r' + response.d[i].Designation + '\rInteractions Count: ' + response.d[i].Count],
                        ]);
                    }


                    // Create the chart.
                    var chart = new google.visualization.OrgChart(document.getElementById('orgchart'));
                    // Draw the chart, setting the allowHtml option to true for the tooltips.
                    chart.draw(data, { allowHtml: true });
                },
                failure: function (response) {

                }
            });
        }

        function ViewDetails(EmployeeId) {
            $.ajax({
                type: "POST",
                url: "Default.aspx/EmployeeSession",
                data: '{"employeeid":"' + EmployeeId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.location.href = './EmployeeDetails.aspx';
                },
                failure: function (response) {

                }
            });
        }
    </script>
    <style>
        .btn {
            margin-top: 25px;
            padding: 6px 31px;
            border-radius: 10%;
            background: orange;
            border: black;
            font-size: 20px;
        }

        .orgdiv {
            width: 120em;
            height: 46em;
            overflow-x: auto;
            overflow-y: auto;
            margin-top: -15em;
            margin-left: -59em;
            border: 1px solid #ccc;
            background-color: #f3f3f3;
            position: fixed;
            top: 50%;
            left: 50%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <h3>Org Charts</h3>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 36px 159px;">
        <div class="col-md-4 col-lg-4">
            <div class="form-group" style="margin:0px 150px">
                <div style="margin:0px 75px; width:140px">
                    <label>Select Company</label>
                </div>
                
                <select class="form-control" id="ddlCompany" name="ddlCompany" onchange="BindDepartments();" style="margin:0px 74px">
                </select>
            </div>
        </div>

        <div class="col-md-4 col-lg-4" id="divdept">
            <div class="form-group">
                <div>
                    <label>Select Department</label>
                </div>
                <div style="margin-top:-24px">
                    <select class="form-control" id="ddlDepartment" name="ddlDepartment" multiple="multiple">
                    </select>
                </div>

            </div>
        </div>
        
        <div class="col-md-4 col-lg-4">
            <div class="form-group">
                <label></label>
                <button type="button" class="btn" onclick="DrawChart();">View</button>
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>




    <div class="row">
        <div id="orgchart" class="orgdiv">
        </div>
    </div>
</asp:Content>
